import {ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LookupService} from './microfrontends/lookup.service';
import {MicroFrontend} from './microfrontends/micro-frontend';
import {buildRoutes} from '../menu-utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  isAuthenticated = false;
  currentUserChannel: BroadcastChannel;
  microFrontends: MicroFrontend[] = [];

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private router: Router, private lookupService: LookupService,
              private zone: NgZone) {
    this.currentUserChannel = new BroadcastChannel("currentUser");
  }

  async ngOnInit() {
    this.microFrontends = await this.lookupService.lookup();
    const routes = buildRoutes(this.microFrontends);
    this.router.resetConfig(routes);
    this.currentUserChannel.onmessage = (e) => {
      this.isAuthenticated = e.data !== undefined && e.data !== null;
      this.changeDetectorRef.detectChanges();
    };
  }

  ngOnDestroy() {
    this.currentUserChannel.close();
  }

  getMfs() {
    return this.microFrontends.filter(m => m.remoteName !== 'authentication');
  }

  route(path: string) {
    this.zone.run(() => this.router.navigate([path]));
  }
}
