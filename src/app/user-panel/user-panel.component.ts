import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'authentication-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit, OnDestroy {
  currentUser = {username: '', password: ''};
  authChannel: any;
  userChannel: any;

  constructor(private router: Router, private zone: NgZone) {
  }

  ngOnInit(): void {
    const channel = new BroadcastChannel("currentUser");
    channel.onmessage = (e) => {
      this.currentUser = {username: e.data ? e.data : '', password: ''};
    };
  }

  onLogout() {
    this.currentUser = {username: '', password: ''};
    this.authChannel = new BroadcastChannel("authentication");
    this.authChannel.postMessage("logout");
    this.zone.run(() => this.router.navigate(['/']));
  }

  ngOnDestroy() {
    this.authChannel.close();
    this.userChannel.close();
  }
}
