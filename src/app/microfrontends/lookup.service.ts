import {MicroFrontend} from './micro-frontend';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

interface Importmap {
  imports: { [key: string]: string }
}

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  constructor(private http: HttpClient) {
  }

  async lookup(): Promise<MicroFrontend[]> {
    if (environment.production) {
      const importmap = (await this.http.get<Importmap>(environment.importmapUrl).toPromise()).imports;

      const mfs: MicroFrontend[] = [];
      LookupService.getImports(importmap).forEach(entry => {
        const key = entry[0];
        const name = key.split('/')[1];
        const displayName = name[0].toUpperCase() + name.substr(1);
        mfs.push({
          remoteEntry: entry[1],
          remoteName: name,
          exposedModule: './Module',
          displayName: displayName,
          routePath: name,
          ngModuleName: `${displayName}Module`
        })
      })
      return mfs;
    }
    return Promise.resolve([
      {
        remoteEntry: 'http://localhost:3000/remoteEntry.js',
        remoteName: 'dashboard',
        exposedModule: './Module',
        displayName: 'Dashboard',
        routePath: 'dashboard',
        ngModuleName: 'DashboardModule'
      },
      {
        remoteEntry: 'http://localhost:3001/remoteEntry.js',
        remoteName: 'authentication',
        exposedModule: './Module',
        displayName: 'Authentication',
        routePath: 'authentication',
        ngModuleName: 'AuthenticationModule'
      },
      {
        remoteEntry: 'http://localhost:3002/remoteEntry.js',
        remoteName: 'article',
        exposedModule: './Module',
        displayName: 'Article',
        routePath: 'article',
        ngModuleName: 'ArticleModule'
      },
      {
        remoteEntry: 'http://localhost:3003/remoteEntry.js',
        remoteName: 'sales',
        exposedModule: './Module',
        displayName: 'Sales',
        routePath: 'sales',
        ngModuleName: 'SalesModule'
      }
    ] as MicroFrontend[]);
  }

  private static getImports(importmap: { [p: string]: string }) {
    return Object.entries(importmap)
      .filter(entry =>
        !entry[0].endsWith('/') &&
        entry[0].startsWith('@moduleFederation')
      );
  }
}
