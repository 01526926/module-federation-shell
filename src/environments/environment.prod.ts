export const environment = {
  production: true,
  importmapUrl: 'https://storage.googleapis.com/mf_static_files/importmap.json'
};
