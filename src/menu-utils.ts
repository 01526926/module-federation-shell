import {MicroFrontend} from './app/microfrontends/micro-frontend';
import {Routes} from '@angular/router';
import {loadRemoteModule} from '@angular-architects/module-federation';
import {APP_ROUTES} from './app/app.routes';

export function buildRoutes(options: MicroFrontend[]): Routes {
  const lazyRoutes: Routes = options.map(o => ({
    path: o.routePath,
    loadChildren: () => loadRemoteModule(o).then(m => m[o.ngModuleName])
  }));

  return [...APP_ROUTES, ...lazyRoutes];
}
